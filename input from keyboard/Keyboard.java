import java.io.*;
class Keyboard{
	public static void main(String[] args) throws IOException{
		InputStreamReader cin = null;
		cin = new InputStreamReader(System.in);
		System.out.println("Enter any key from the keyboard: ");
		char c = (char) cin.read();
		System.out.println(c);
		cin.close();
	}
}
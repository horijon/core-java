class CloningReturnsObjectOfObjectClass implements Cloneable{  
int rollno;  
String name;  
  
CloningReturnsObjectOfObjectClass(int rollno,String name){  
this.rollno=rollno;  
this.name=name;  
}  

/*
public Object clone()throws CloneNotSupportedException{  
return super.clone();  
}  
*/
  
public static void main(String args[]){  
try{  
CloningReturnsObjectOfObjectClass s1=new CloningReturnsObjectOfObjectClass(101,"kshitij");  
  
CloningReturnsObjectOfObjectClass s2=(CloningReturnsObjectOfObjectClass)s1.clone();  
  
System.out.println(s1.rollno+" "+s1.name);  
System.out.println(s2.rollno+" "+s2.name);  
  
}catch(CloneNotSupportedException c){
	System.out.println("Catched Exception");
	c.printStackTrace();
}  
  
}  
}  

class StringTest{
	public static void main(String[] args){
		System.out.println("Substring/r/n");
		String str= new String("quick brown fox jumps over the lazy dog");
		System.out.println("Substring starting from index 15:");
		System.out.println(str.substring(15));
		System.out.println("Substring starting from index 15 and ending at 20:");
		System.out.println(str.substring(15, 20));
		
		System.out.println("+ Operand concatenation effects/r/n");
		System.out.println("1" + 2 + 7);
		System.out.println(1 + 2 + "7");
		
		System.out.println("StringBuilder\r\n");
		
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("String Builder Appended");
		stringBuilder.replace(0,stringBuilder.length(), "Hello World");
		System.out.println(stringBuilder);
	}
}
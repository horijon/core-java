import java.util.Properties;
class Home{
	public static void main(String[] args){
		System.out.println("Path = " + System.getProperty("java.home"));
		System.out.println("Class Path = " + System.getProperty("java.class.path"));
		Properties props = System.getProperties();
		props.list(System.out);
	}
}
import java.io.BufferedReader;
import java.io.InputStreamReader;
class Test{
	public static void main(String[] args) throws Exception{
		String line;
		Process p = Runtime.getRuntime().exec("php test.php");
		BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
		while((line = input.readLine()) != null)
			System.out.println(line);
		input.close();
	}
}
import java.lang.reflect.*;  
class ReflectionPrivateMethodInvocation{  
	public static void main(String args[])throws Exception{  
		Class c=ReflectionPrivateMethod.class;  
		Object obj=c.newInstance();  
  
		Method m=c.getDeclaredMethod("multiply",new Class[]{String.class, int.class});  
		m.setAccessible(true);  
		m.invoke(obj,"Hello ", 5);
	}
}

class ReflectionPrivateMethod{  
	private void multiply(String x, int y){
		System.out.println(x + y);
	}  
}  
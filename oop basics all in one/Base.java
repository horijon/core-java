public class Base
{
	static{
		System.out.println("Base static block\r\n");
	}
	
	{
		System.out.println("Base instance block\r\n");
	}
	
	Base(){
		super();
		System.out.println("Base constructor\r\n");
	}
	
	public void methodOverridding()
	{
		System.out.println("Base Overridden Method\r\n");
	}
	
	public static void methodHidding(){
		System.out.println("Base static method called as the reference is of type Base(same goes for the interface as well, interface also can have static methods which can only be called by the interface itself)\r\nIt hid the overridden method so this is called Method Hidding\r\n");
	}
}

class Derived extends Base
{
	static{
		System.out.println("Derived static block\r\n");
	}
	
	{
		System.out.println("Derived instance block\r\n");
	}
	
	Derived(){
		super();
		System.out.println("Derived constructor\r\n");
	}
	
	public void methodOverridding()
	{
		System.out.println("For non-static methods always the derived method is called\r\nThis is called Method Overridding\r\n");
	}
	
	public static void methodHidding(){
		System.out.println("Derived Static method called As the reference is of Type Derived\r\n");
	}
}

class Test
{
	public static void main(String[] args)
	{
		
		Base bc = new Derived();
		System.out.println("All static blocks invoked once\r\n");
		
		System.out.println("All constructors and instance blocks inside constructors are invoked each time a new object is created\r\n");

		System.out.println("Instance initializer block is copied inside the constructor just after the super() invocation\r\n");
		
		System.out.println("First static block of Base is called and then static block of derived is called in case if the object is of type derived\r\n");
		
		System.out.println("If any two methods having the same signature overrided\r\n1) then they must both be of same type(static or non-static) and\r\n2) they both must have same return type\r\n");
		
		System.out.println();
		
		bc.methodOverridding();
		bc.methodHidding();
		
		/*
		System.out.println();
		
		b.methodOverridding();
		b.methodHidding();
		
		System.out.println();
		
		d.methodOverridding();
		d.methodHidding();
		*/
		
		System.out.println("These method calls example applies for runtime polymorphism\r\nOtherwise the calls are according to its own class and/or inheritance rule\r\n");
		
		System.out.println("New Line and Return Carriage Effects");
		
		System.out.println("A1\nA2\rA3A4\rA5A6");
		System.out.println("B1\rB2B3");
		System.out.println("C1\n\rC2C3");
		System.out.println("D1\r\nD2D3");
		System.out.println("E1\nE2E3\r\n");
		
		System.out.println("super and this keywords are not allowed inside static constext\r\nIn case of constructors, it must be the first statement and only one of either super or this can be used\r\n");
		
		System.out.println("super or this cannot be used inside methods also but can be used inside constructors, eg super(1) inside default constructor of derived class which calls parameterized constructor of Base class\r\n");
	}
	
}
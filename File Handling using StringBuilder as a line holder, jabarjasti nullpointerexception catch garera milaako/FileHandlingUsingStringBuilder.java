import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
//import java.io.BufferedWriter;
import java.io.PrintWriter;
import java.io.IOException;
class FileHandlingUsingStringBuilder{
	public static void main(String[] args) throws IOException{
		
		System.out.println("\r\nUsing StringBuilder instead of String unable to handle file without throwing NullPointerException\r\n\r\nAt the last line read where bufferedReader.readLine() gives null value, on invoking the replace method of StringBuilder throws NullPointerException\r\n\r\nJabarjasti milaaiyo, object creation of string in a loop hatauna ko laagi, NullPointerException ko balidaan diyiyo\r\n");
		
		BufferedReader bufferedReader = new BufferedReader(new FileReader("sampleInput.txt"));
		//BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("sampleOutput.txt"));
		PrintWriter printWriter = new PrintWriter(new FileWriter("sampleOutput.txt"));
		
		StringBuilder line = new StringBuilder(bufferedReader.readLine());
		try{
			while(!line.toString().equals("null")){
				//bufferedWriter.write(line + "\r\n");				
				printWriter.println(line.toString());
				line.replace(0,line.length(),bufferedReader.readLine());
			}
	} catch(IOException | NullPointerException e) {
			//e.printStackTrace();
		} finally {
			bufferedReader.close();
			//bufferedWriter.flush();
			//bufferedWriter.close();
			printWriter.flush();
			printWriter.close();
		}
	}
}
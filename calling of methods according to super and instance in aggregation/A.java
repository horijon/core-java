class A {
  void foo() {
    // "this" also known under the names "current", "me" and "self" in other languages
    this.bar();
  }

  void bar() {
    System.out.println("a.bar");
  }
};

class B extends A{
  private A a; // delegation link

  public B(A a) {
    this.a = a;
  }

  void foo() {
    a.foo(); // call foo() on the a-instance
	super.foo(); // call foo() on the b-instance
  }

  void bar() {
    System.out.println("b.bar");
  }
};

class TestAB{
	public static void main(String[] args){
		A a = new A();
		B b= new B(a); // establish delegation between two objects
		a.foo();
		b.foo();
	}
}
class Test<T>{ //generic class can use T inside class anywhere
	T t;
	public void setValue(T t){ // as class is generic, no need to make method generic using <T> before return type explicitly
		this.t = t;
	}
	public T getValue(){
		return t;
	}

	public <E> void printSomething(E e){ // generic method using E as parameter, no need for making class generic
		System.out.println(e);
	}
}

class Tester{
	public static void main(String[] args){
		Test<String> test = new Test<>();
		Test<Integer> testInteger = new Test<>();
		test.setValue("Hello World");
		testInteger.setValue(5);
		System.out.println(test.getValue());
		System.out.println(testInteger.getValue());
		test.printSomething("Hello kshitij");
		test.printSomething(777);
	}
}
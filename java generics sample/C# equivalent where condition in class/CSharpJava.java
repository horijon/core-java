import java.util.*;
public class CSharpJava<T extends Number, K  >
{
	public void doSomething(){
		System.out.println("Compilation and Execution successful");
	}
}

class CSharpJavaTest{
	public static void main(String[] args){
		CSharpJava<Integer,String> cSharp = new CSharpJava<>();
		cSharp.doSomething();
	}	
}
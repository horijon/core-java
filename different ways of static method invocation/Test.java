class Test{  
	static{
		System.out.println("Static Block");
		//System.exit(0);
	}
	static int cube(int x){
		System.out.println("Static method");
		return x*x*x;  
	}  
	
	Test(){
		System.out.println("Constructor");
	}
  
	public static void main(String args[]){  
		Test c = new Test();
		int result1 = c.cube(2);
		int result2 = Test.cube(3);
		int result3 = cube(4);
		System.out.println(result1);
		System.out.println(result2);
		System.out.println(result3);
	}  
}  
import java.io.BufferedReader;
import java.io.InputStreamReader;
class Test{
	public static void main(String[] args) throws Exception	{
		//Runtime.getRuntime().exec("notepad");
		String line;
		Process p = Runtime.getRuntime().exec("python test.py");
		BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
		while((line = input.readLine()) != null)
			System.out.println(line);
		input.close();
	}
}